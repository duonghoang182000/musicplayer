package com.mindorks.retrofit.musicplayer.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Song(
    @PrimaryKey val songId: Long

)
